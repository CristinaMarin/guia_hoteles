$ (function (){
	   $("[data-toggle='tooltip']").tooltip();
	   $("[data-toggle='popover']").popover();
	   $('.carousel').carousel({
         interval: 2000	   
	   });
	 $('#reserva').on('show.bs.modal', function (e) {
		console.log ('el modal reservas se esta mostrando');
		$('#reservaBtn').removeClass('btn-outline-success');
		$('#reservaBtn').addClass('btn-primary');
		$('#reservaBtn').prop('disabled', true);	
		});
	 $('#reserva').on('shown.bs.modal', function (e) {
		console.log ('el modal reservas se mostró')
		});
	$('#reserva').on('hide.bs.modal', function (e) {
		console.log ('el modal reservas se oculta')
		});
	 $('#reserva').on('hidden.bs.modal', function (e) {
		console.log ('el modal reservas se ocultó');
		$('#reservaBtn').prop('disabled', false);
		});
	   });